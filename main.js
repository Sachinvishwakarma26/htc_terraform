
'use strict'
import axios

exports.handler = function(event, context, callback) {
  var response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html; charset=utf-8'
    },
    body: '<p>Hello HTC Team! This is first terraform script</p>'
  }
  callback(null, response)
}
